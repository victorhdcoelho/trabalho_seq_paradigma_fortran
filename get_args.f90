PROGRAM get_agrs
      INTEGER :: i, n
      INTEGER :: result
      INTEGER :: pass_arg, num_arg
      INTEGER :: passes
      INTEGER :: init, end_loop, incre 
      REAL :: init_r, end_loop_r, incre_r
      INTEGER :: flag, zeros
      CHARACTER(len=10) :: reads
      CHARACTER(len=10) :: reads_a
      CHARACTER(len=20) :: arg
      CHARACTER(len=10) :: sep
      CHARACTER(len=400) :: valor
      CHARACTER(len=400) :: valor2
      CHARACTER(len=400) :: compare
      CHARACTER(len=20) :: zero  
      INTEGER :: exec
      i = 1
      exec = 0
      sep = '\n'
      pass_arg = 0
      init = 0
      end_loop =0
      incre = 0
      init_r =0
      incre_r = 0
      end_loop_r = 0
      flag = 0
      num_arg = COMMAND_ARGUMENT_COUNT()
      DO
        CALL get_command_argument(i, arg)
        IF (LEN_TRIM(arg) .eq. 0) EXIT

        IF((arg .eq. '--help') .and. (i .eq. 1)) THEN
                WRITE(*, *) "Uso: seq [OPÇÃO]... ÚLTIMO"
                WRITE(*, *) "ou: seq [OPÇÃO]... PRIMEIRO ÚLTIMO"
                WRITE(*, *) "ou: seq [OPÇÃO]... PRIMEIRO INCREMENTO ÚLTIMO"
                WRITE(*, *) "Emite números de PRIMEIRO até ÚLTIMO, de INCREMENTO em INCREMENTO."
                WRITE(*, *)
                WRITE(*, *) "Argumentos obrigatórios para opções longas também o são para opções curtas."
                WRITE(*, *) "-f, --format=FORMATO     usa o FORMATO de ponto flutuante no estilo printf"
                WRITE(*, *) "-s, --separator=TEXTO    usa TEXTO para separar números (padrão: \n)"
                WRITE(*, *) "-w, --equal-width        equaliza a largura preenchendo com zeros à esquerda"
                WRITE(*, *) "--help     mostra esta ajuda e sai"
                WRITE(*, *) "--version  informa a versão e sai"
                WRITE(*, *)
                WRITE(*, *) "Se são omitidos PRIMEIRO ou INCREMENTO, eles são tratados como 1. Isto é,"
                Write(*, *) "um INCREMENTO omitido é tratado como 1 quando ÚLTIMO é menor que PRIMEIRO."
                Write(*, *) "A sequência de números termina quando a soma do número atual e o INCREMENTO"
                write(*, *) "se tornasse maior que ÚLTIMO."
                write(*, *) "PRIMEIRO, INCREMENTO e ÚLTIMO são interpretados como valores em ponto"
                write(*, *) "flutuante."
                write(*, *) "INCREMENTO geralmente é positivo se PRIMEIRO for menor que ÚLTIMO, e"
                write(*, *) "INCREMENTO geralmente é negativo se PRIMEIRO for maior que ÚLTIMO."
                write(*, *) "INCREMENTO não deve ser 0; PRIMEIRO, INCREMENTO e ÚLTIMO não podem ser NaN."
                write(*, *) 'FORMATO deve estar apropriado para mostrar um argumento do tipo "double";'
                write(*, *) "por padrão, é %.PRECf se PRIMEIRO, INCREMENTO e ÚLTIMO forem números"
                write(*, *) "decimais de ponto fixo com precisão máxima PREC, e é %g caso contrário."
                write(* ,*)
                write(*, *) "Página de ajuda do GNU coreutils: <https://www.gnu.org/software/coreutils/>"
                write(*, *) "Relate erros de tradução do seq: <https://translationproject.org/team/pt_BR.html>"
                write(*, *) "Documentação completa em: <https://www.gnu.org/software/coreutils/seq>"
                write(*, *) 'ou disponível localmente via: info "(coreutils) seq invocation"'
                exit
        ENDIF
        
        IF((arg .eq. '--version') .and. (i .eq. 1)) THEN
                WRITE(*, *) "seq (GNU coreutils) 8.30"
                WRITE(*, *) "Copyright (C) 2018 Free Software Foundation, Inc."
                WRITE(*, *) "Licença GPLv3+: GNU GPL versão 3 ou posterior <https://gnu.org/licenses/gpl.html>"
                WRITE(*, *) "Este é um software livre: você é livre para alterá-lo e redistribuí-lo."
                WRITE(*, *) "NÃO HÁ QUALQUER GARANTIA, na máxima extensão permitida em lei."
                WRITE(*, *) "Escrito por Ulrich Drepper."
                exit
        ENDIF
        if(INDEX(arg, '-w') .ne. 0) then
                pass_arg = pass_arg + 1
                result = INDEX(arg, '-w')
                result = result + 2
                zeros = 1
                i = i + 1
                call get_command_argument(i, arg) 
        ENDIF

        IF(INDEX(arg, '-s') .ne. 0) THEN
                pass_arg = pass_arg + 1
                result = INDEX(arg, '-s')
                result = result + 2
                sep = arg(result: len(arg)-1)
                if(sep .eq. '') then
                        i = i + 1
                        pass_arg = pass_arg + 1
                        call get_command_argument(i, arg)
                        sep = arg
                endif
        ELSE
                if(num_arg - pass_arg .ne. 0)then
                        read(arg, '(A)') compare
                        if (INDEX(compare, '.') .ne. 0) then
                                reads = '(F10.2)'
                                reads_a = '(F10.2, A)'
                                flag = 0
                        else
                                reads = '(I8)'
                                reads_a = '(I0, A)'
                                flag = 1
                        endif
                endif
                if(num_arg - pass_arg .eq. 1) then
                        if(flag .eq. 1) then
                            read(arg, reads) end_loop
                            if (zeros == 1) then
                                write(valor,'(I0)') end_loop
                                write(zero, "(I0.0)")LEN_TRIM(valor) 
                                reads =("(I8."//trim(zero))//")"
                                read(arg, reads) end_loop
                                reads_a = ("(I0."//trim(zero))//", A)"
                            end if
                        else
                            read(arg, reads) end_loop_r
                        endif
                endif
                if(num_arg - pass_arg .eq. 2) then
                        if(flag .eq. 1) then
                            read(arg, reads) init
                            i = i + 1
                            call get_command_argument(i, arg) 
                            read(arg, reads) end_loop
                            if(zeros == 1) then
                                write(valor,'(I0)') end_loop
                                write(zero, "(I0.0)")LEN_TRIM(valor) 
                                reads =("(I8."//trim(zero))//")"
                                read(arg, reads) end_loop
                                reads_a = ("(I0."//trim(zero))//", A)"
                            end if
                        else
                            read(arg, reads) init_r
                            i = i + 1
                            call get_command_argument(i, arg)
                            read(arg, reads) end_loop_r
                        endif
                endif
                if(num_arg - pass_arg .eq. 3) then
                        if(flag .eq. 1) then
                            read(arg, reads) init
                            i = i + 1
                            call get_command_argument(i, arg)
                            read(arg, reads) incre
                            i = i + 1
                            call get_command_argument(i, arg)
                            read(arg, reads) end_loop
                            write(valor,'(I0)') end_loop
                            write(valor2, '(I0)') init

                            if(LEN_TRIM(valor) > LEN_TRIM(valor2)) then
                                     if(zeros == 1) then
                                        write(valor,'(I0)') end_loop
                                        write(zero, "(I0.0)")LEN_TRIM(valor) 
                                        reads =("(I8."//trim(zero))//")"
                                        read(arg, reads) end_loop
                                        reads_a = ("(I0."//trim(zero))//", A)"
                                    end if
   
                            else
                                    if(zeros == 1) then
                                        write(valor,'(I0)') init
                                        write(zero, "(I0.0)")LEN_TRIM(valor) 
                                        reads =("(I0."//trim(zero))//")"
                                        read(arg, reads) init
                                        reads_a = ("(I0."//trim(zero))//", A)"
                                    end if
                            endif


                        else
                            read(arg, reads) init_r
                            i = i + 1
                            call get_command_argument(i, arg)
                            read(arg, reads) incre_r
                            i = i + 1
                            call get_command_argument(i, arg)
                            read(arg, reads) end_loop_r

                        endif
                endif
        ENDIF
        i = i + 1
      END DO
      if(end_loop .lt. init .and. incre .ge. 0 .and. end_loop_r .lt. init_r .and. incre_r .ge. 0) then
              call exit(1)
      endif
      if (end_loop .ne. 0 .and. init .ne. 0 .and. incre .eq. 0 .or. end_loop_r .ne. 0 .and. init_r .ne. 0 .and. incre_r .eq. 0) then
              if(flag .eq. 1) then
                      incre = 1
                      do while (init .lt. end_loop + incre) 
                            write(valor, reads) init
                            if(init  == end_loop) then
                                    if(sep .eq. '\n') then
                                        write(6, '(A)')trim(adjustl(valor))
                                    else
                                        write(6, '(A)', advance='no')trim(adjustl(valor))
                                    endif
                            else
                                if(sep .eq. '\n') then
                                    write(6, '(A)')valor
                                else
                                    write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                                endif
                            endif
                            init = init + incre
                       end do
              else
                    incre_r = 1.0
                    do while (init_r .lt. end_loop_r + incre_r)
                            write(valor, reads) init_r
                            if(init_r  == end_loop_r) then
                                    if(sep .eq. '\n') then
                                        write(6, '(A)')trim(adjustl(valor))
                                    else
                                        write(6, '(A)', advance='no')trim(adjustl(valor))
                                    endif
                            else
                                if(sep .eq. '\n') then
                                    write(6, '(A)')trim(adjustl(valor))
                                else
                                    write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                                endif
                            endif
                            init_r = init_r + incre_r
                       end do
              endif
              
      else if (incre .ne. 0 .or. init_r .ne. 0) then
              if(flag .eq. 1) then
                do while (init .lt. end_loop + incre)
                    write(valor, reads) init
                    if(init  == end_loop) then
                            if(sep .eq. '\n') then
                                write(6, '(A)')trim(adjustl(valor))
                            else
                                write(6, '(A)', advance='no')trim(adjustl(valor))
                            endif
                    else
                        if(sep .eq. '\n') then
                            write(6, '(A)')trim(adjustl(valor))
                        else
                            write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                        endif
                    endif
                    init = init + incre
               end do
               do while (end_loop + incre .lt. init)
                    write(valor, reads) init
                    if(init  == end_loop) then
                            if(sep .eq. '\n') then
                                write(6, '(A)')trim(adjustl(valor))
                            else
                                write(6, '(A)', advance='no')trim(adjustl(valor))
                            endif
                    else
                        if(sep .eq. '\n') then
                            write(6, '(A)')trim(adjustl(valor))
                        else
                            write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                        endif
                    endif
                    init = init + incre
               end do
              else
                 do while (init_r .lt. end_loop_r + incre_r)
                    exec = 1
                    write(valor, reads) init_r
                    if(init_r  > end_loop_r) then
                            if(sep .eq. '\n') then
                                write(6, '(A)')trim(adjustl(valor))
                            else
                                write(6, '(A)', advance='no')trim(adjustl(valor))
                            endif
                    else
                        if(sep .eq. '\n') then
                            write(6, '(A)')valor
                        else
                            write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                        endif
                    endif
                    init_r = init_r + incre_r
               end do
               if(exec == 0) then
                       do while (end_loop_r + incre_r .lt. init_r)
                            write(valor, reads) init_r
                            if(init_r  > end_loop_r + incre_r) then
                                    if(sep .eq. '\n') then
                                        write(6, '(A)')trim(adjustl(valor))
                                    else
                                        write(6, '(A)', advance='no')trim(adjustl(valor))
                                    endif
                            else
                                if(sep .eq. '\n') then
                                    write(6, '(A)')valor
                                else
                                    write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                                endif
                            endif
                            init_r = init_r + incre_r
                       end do
               end if
              endif
              

      else
              if(flag .eq. 1)then 
                      init = 1
                      incre = 1
                      do while (init .lt. end_loop + incre)
                            write(valor, reads) init
                            if(init  == end_loop) then
                                    if(sep .eq. '\n') then
                                        write(6, '(A)')trim(adjustl(valor))
                                    else
                                        write(6, '(A)', advance='no')trim(adjustl(valor))
                                    endif
                            else
                                if(sep .eq. '\n') then
                                    write(6, '(A)')trim(adjustl(valor))
                                else
                                    write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                                endif
                            endif
                            init = init + incre
                       end do
              else
                      init_r = 1.0
                      incre_r = 1.0
                      do while (init_r .lt. end_loop_r + incre_r) 
                            write(valor, reads) init_r
                            if(init_r  == end_loop_r) then
                                    if(sep .eq. '\n') then
                                        write(6, '(A)')trim(adjustl(valor))
                                    else
                                        write(6, '(A)', advance='no')trim(adjustl(valor))
                                    endif
                            else
                                if(sep .eq. '\n') then
                                    write(6, '(A)')trim(adjustl(valor))
                                else
                                    write(6, '(A, A)', advance='no')trim(adjustl(valor)) ,trim(adjustl(sep))
                                endif
                            endif
                            init_r = init_r + incre_r
                       end do
              endif 
      end if
      if(sep .ne. '\n') then
        write(*, *)
      endif
END PROGRAM  
